package fr.sedoo.rbv.metadataws;

import java.io.File;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.apache.commons.io.FileUtils;

import fr.sedoo.commons.metadata.utils.domain.MetadataTools;
import fr.sedoo.commons.metadata.utils.domain.SedooMetadata;
import fr.sedoo.commons.web.dao.WebConfigVersionDAO;

@Path("/RBVTo19139")
public class RBVTo19139Service {

	@GET
	@Path("/version")
	public Response getVersion() 
	{
		WebConfigVersionDAO dao = new WebConfigVersionDAO();
		String version = dao.getVersion();
		return Response.status(200).entity(version).build();
	}
	
	@GET
	@Path("/sampleRBV")
	public Response getSampleRBV() 
	{
		try
		{
			RBVMetadata testMetadata = MetadataSampleProvider.getTestMetadata();
			String rbv = RBVMarshaller.toRBV(testMetadata);
			return Response.status(200).entity(rbv).build();
		}
		catch (Exception e)
		{
			String str ="Une erreur est survenue : "+e.getMessage();
			return Response.status(200).entity(str).build();
		}
	}
	
	
	
	@POST
	@Path("/post/text")
	@Consumes("application/x-www-form-urlencoded")
	public Response post(@FormParam("src") String src) 
	{
		try
		{
			SedooMetadata metadata = RBVTools.fromRBV(src);
			String str = MetadataTools.toISO19139(metadata);
			return Response.status(200).entity(str).build();
		}
		catch (Exception e)
		{
			String str ="Une erreur est survenue : "+e.getMessage();
			return Response.status(200).entity(str).build();
		}
	}
	
	@POST
	@Path("/post/download")
	@Consumes("application/x-www-form-urlencoded")
	@Produces("application/xml")
	public Response download(@FormParam("src") String src) 
	{
		try
		{
			SedooMetadata metadata = RBVTools.fromRBV(src);
			String str = MetadataTools.toISO19139(metadata);
			File tmp = File.createTempFile("rbv", ".xml");
			FileUtils.writeStringToFile(tmp, str);
			
			ResponseBuilder response = Response.ok((Object) tmp);
			response.header("Content-Disposition", "attachment; filename=\"rbv.xml\"");
			return response.status(200).build();
			
		}
		catch (Exception e)
		{
			String str ="Une erreur est survenue : "+e.getMessage();
			return Response.status(500).entity(str).build();
		}
	}
	
	
	
	
	/*@POST
	@Path("/post")
	@Consumes("application/x-www-form-urlencoded")
	@Produces("text/plain")
	public Response post(@FormParam("rbvstring") String rbvstring) 
	{
		
		
		String output = "Jersey say : " + name;
		//return Response.status(200).entity(output).build();
		File file = new File("/home/andre/developpement/workspace-rbv/metadataws/pom.xml");
		
		File 
		
		ResponseBuilder response = Response.ok((Object) file);
		response.header("Content-Disposition", "attachment; filename=\"file_from_server.log\"");
		return response.build();
	}*/
	
	
	/*@GET
	@Path("/post")
	@Produces("text/plain")
	public Response getFile() {
		File file = new File("/home/andre/developpement/workspace-rbv/metadataws/pom.xml");
 
		ResponseBuilder response = Response.ok((Object) file);
		response.header("Content-Disposition", "attachment; filename=\"file_from_server.log\"");
		return response.build();
 
	}*/
}