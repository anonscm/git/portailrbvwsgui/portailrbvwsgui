package fr.sedoo.rbv.metadataws;

import java.io.File;
import java.io.IOException;
import java.net.URI;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriBuilder;

import org.apache.commons.io.FileUtils;
import org.junit.Test;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.core.util.MultivaluedMapImpl;

import fr.sedoo.commons.metadata.utils.domain.SedooMetadata;

public class TestRBVTo19139Service {

	
	private static URI getBaseURI() {
		return UriBuilder.fromUri("http://localhost:9080/metadataws").build();
	}
	
	@Test
	public void testVersion()
	{
		System.out.println("-- test Version --");
		ClientConfig config = new DefaultClientConfig();
		Client client = Client.create(config);
		WebResource service = client.resource(getBaseURI());
		String result = service.path("rest").path("RBVTo19139").path("version").accept(MediaType.TEXT_HTML).get(String.class).toString();
		System.out.println(result);
	}
	
	@Test
	public void testPostDownload() throws Exception
	{
		System.out.println("-- test Postdownload --");
		MultivaluedMap formData = new MultivaluedMapImpl();
		RBVMetadata metadata = MetadataSampleProvider.getTestMetadata();
		String str1 = RBVTools.toRBV(metadata);
		formData.add("src", str1);
		ClientConfig config = new DefaultClientConfig();
		Client client = Client.create(config);
		WebResource service = client.resource(getBaseURI());
		ClientResponse response = service.path("rest").path("RBVTo19139").path("post").path("download").type(MediaType.APPLICATION_FORM_URLENCODED_TYPE).post(ClientResponse.class, formData);
		File s= response.getEntity(File.class);
		String readFileToString = FileUtils.readFileToString(s);
		System.out.println(readFileToString);
	}
	
	@Test
	public void testPostText() throws Exception
	{
		System.out.println("-- test PostText --");
		MultivaluedMap formData = new MultivaluedMapImpl();
		RBVMetadata metadata = MetadataSampleProvider.getTestMetadata();
		String str1 = RBVTools.toRBV(metadata);
		formData.add("src", str1);
		ClientConfig config = new DefaultClientConfig();
		Client client = Client.create(config);
		WebResource service = client.resource(getBaseURI());
		ClientResponse response = service.path("rest").path("RBVTo19139").path("post").path("text").type(MediaType.APPLICATION_FORM_URLENCODED_TYPE).post(ClientResponse.class, formData);
		String isoString = response.getEntity(String.class);
		
		System.out.println(isoString);
	}
}