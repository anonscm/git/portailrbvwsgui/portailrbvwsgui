package fr.sedoo.rbv.metadataws;

import java.net.URI;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriBuilder;

import junit.framework.Assert;

import org.apache.commons.httpclient.HttpStatus;
import org.junit.Test;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.core.util.MultivaluedMapImpl;

import fr.sedoo.commons.metadata.utils.domain.SedooMetadata;
import fr.sedoo.rbv.geonetwork.request.MetadataSummary;

public class TestIntegrationService {

	
	private static URI getBaseURI() {
		return UriBuilder.fromUri("http://localhost:9080/metadataws").build();
	}
	
	@Test
	public void testVersion()
	{
		System.out.println("-- test Version --");
		ClientConfig config = new DefaultClientConfig();
		Client client = Client.create(config);
		WebResource service = client.resource(getBaseURI());
		String result = service.path("rest").path("Integration").path("version").accept(MediaType.TEXT_HTML).get(String.class).toString();
		System.out.println(result);
	}
	
	/**
	 * Test effectuant un certain nombre d'actions élémentaires d'alimentation du portail:
	 *  - Création d'une fiche au format RBV
	 *  - Intégration dans le portail
	 *  - Récupération de l'uuid
	 *  - Vérification de la présence et du contenu
	 *  - Modification  de la fiche
	 *  - Mise à jour dans le portail
	 *  - Vérification du contenu modifié
	 *  
	 * @throws Exception
	 */
	@Test
	public void testBasicLifeCycle() throws Exception
	{
		RBVMetadata metadata = MetadataSampleProvider.getTestMetadata();
		String content = RBVTools.toRBV(metadata);
	
		MultivaluedMap formData = new MultivaluedMapImpl();
		formData.add("src", content);
		formData.add("format", IntegrationService.RBV_FORMAT_NAME);
		formData.add("login", "test");
		formData.add("password", "sedootest");
		
		ClientConfig config = new DefaultClientConfig();
		Client client = Client.create(config);
		WebResource service = client.resource(getBaseURI());
		ClientResponse response = service.path("rest").path("Integration").path("addWithoutId").type(MediaType.APPLICATION_FORM_URLENCODED_TYPE).post(ClientResponse.class, formData);
		int status = response.getStatus();
		Assert.assertEquals("L'opération doit s'être bien déroulée", HttpStatus.SC_OK, status);
		String uuid = response.getEntity(String.class);
		Assert.assertNotNull("L'identifiant doit être présent", uuid);
		
		
		String result = service.path("rest").path("Integration").path("exist").path(uuid).accept(MediaType.TEXT_HTML).get(String.class).toString();
		Assert.assertEquals("La fiche doit exister", "true", result);
		
		String rawXML = service.path("rest").path("Integration").path("getById").path(uuid).accept(MediaType.TEXT_HTML).get(String.class).toString();
		MetadataSummary summary = MetadataSummary.fromXML(rawXML);
//		Assert.assertEquals("Les données doivent correspondre", metadata.getResourceTitle(), summary.getResourceTitle());
//		metadata.setResourceTitle("Updated "+metadata.getResourceTitle());
		metadata.setUuid(uuid);
		content = RBVTools.toRBV(metadata);
		formData = new MultivaluedMapImpl();
		formData.add("src", content);
		formData.add("format", IntegrationService.RBV_FORMAT_NAME);
		formData.add("login", "test");
		formData.add("password", "sedootest");
		response = service.path("rest").path("Integration").path("addOrUpdateWithId").type(MediaType.APPLICATION_FORM_URLENCODED_TYPE).post(ClientResponse.class, formData);
		status = response.getStatus();
		Assert.assertEquals("L'opération doit s'être bien déroulée", HttpStatus.SC_OK, status);
		rawXML = service.path("rest").path("Integration").path("getById").path(uuid).accept(MediaType.TEXT_HTML).get(String.class).toString();
		summary = MetadataSummary.fromXML(rawXML);
//		Assert.assertEquals("Les données doivent correspondre", metadata.getResourceTitle(), summary.getResourceTitle());
	}
	
	
	
	/*@Test
	public void testExist()
	{
		System.out.println("-- test Exist --");
		ClientConfig config = new DefaultClientConfig();
		Client client = Client.create(config);
		WebResource service = client.resource(getBaseURI());
		String goodId = "7d5ec60c-ca83-408e-a0b6-6871f4df06b0";
		String result = service.path("rest").path("Integration").path("exist").path(goodId).accept(MediaType.TEXT_HTML).get(String.class).toString();
		Assert.assertEquals("La fiche doit exister", "true", result);
		String badId = "xx"+goodId;
		result = service.path("rest").path("Integration").path("exist").path(badId).accept(MediaType.TEXT_HTML).get(String.class).toString();
		Assert.assertEquals("La fiche ne doit pas exister", "false", result);
		result = service.path("rest").path("Integration").path("exist").path(badId).accept(MediaType.TEXT_HTML).get(String.class).toString();
		Assert.assertEquals("La fiche ne doit pas exister", "false", result);
		result = service.path("rest").path("Integration").path("exist").path(badId).accept(MediaType.TEXT_HTML).get(String.class).toString();
		Assert.assertEquals("La fiche ne doit pas exister", "false", result);
		result = service.path("rest").path("Integration").path("exist").path(badId).accept(MediaType.TEXT_HTML).get(String.class).toString();
		Assert.assertEquals("La fiche ne doit pas exister", "false", result);
		result = service.path("rest").path("Integration").path("exist").path(badId).accept(MediaType.TEXT_HTML).get(String.class).toString();
		Assert.assertEquals("La fiche ne doit pas exister", "false", result);
		result = service.path("rest").path("Integration").path("exist").path(badId).accept(MediaType.TEXT_HTML).get(String.class).toString();
		Assert.assertEquals("La fiche ne doit pas exister", "false", result);
	}
	
	@Test
	public void testGedById()
	{
		System.out.println("-- test GedById --");
		ClientConfig config = new DefaultClientConfig();
		Client client = Client.create(config);
		WebResource service = client.resource(getBaseURI());
		String goodId = "7d5ec60c-ca83-408e-a0b6-6871f4df06b0";
		String result = service.path("rest").path("Integration").path("getById").path(goodId).accept(MediaType.TEXT_HTML).get(String.class).toString();
		System.out.println(result);
		Assert.assertTrue("La fiche doit contenir son identifiant", result.contains(goodId));
		Assert.assertTrue("La fiche doit contenir un noeud gmd:MD_Metadata", result.contains("gmd:MD_Metadata"));
	}
	
	@Test
	public void getMultipleById()
	{
		System.out.println("-- test getMultipleById --");
		testGedById();
		testGedById();
		testGedById();
		testGedById();
	}*/
	
	
}