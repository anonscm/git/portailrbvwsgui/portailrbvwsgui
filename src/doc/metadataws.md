# Service web de métadonnées RBV
## Introduction
L'objectif de ce service web est de fournir une possibilité d'alimenter le catalogue du portial RBV de manière automatique pour les observatoires posédant un SI contenant déjà les informations de la fiche de métadonnées.

Le format RBV est décrit [ici](./format.html)

A terme le web service comportera deux principale fonctionnalités:

  -   Conversion Format RBV -> Iso19139
  -   Ajout/modification d'une fiche du portail via son format RBV

## Fonctionnalités offertes


La version actuelle est la **0.0.8**. 

Le paragraphe suivant retrace les fonctionnalités offertes en fonction des différentes versions 
 
 - Version 0.0.8
	- Ajout des mots clés
 - Version 0.0.7
 	- Internationalisation des champs Title, Abstract, UseConditions et publicAccessLimitations
 	- Possiblité d'indiquer un identifiant parent
 	- Possibilité d'indiquer un logo
 	- Possibilité de créér une fiche de type Observatoire, Site expérimental ou Jeu de données
 	- Section Identification : Gestion des types de liens
 	- Section Localisation géographique : possibilité d'indiquer plusieurs zones pour les observatoires
 	- Mise à disposition application (.jar) de traduction 
 
 - Version 0.0.4 :
  	- Alimentation automatique du portail:  
       - Possibilité d'obtenir une fiche au format PDF
 - Version 0.0.3 :  
  	- Alimentation automatique du portail:  
       - Possibilité d'ajouter une fiche de métadonnées dans le portail  
       - Possibilité de modifier une fiche de métadonnées du portail  
 - Version 0.0.2 :
    - Support du format RBV: 
      - Section Empreinte temporelle - complète
      - Section Contraintes d'accès et d'utilisation - complète
      - Section Localisation géographique - complète
      - Section Autres informations sur la donnée - complète
 - Version 0.0.1 : 
    - Mise en place de la conversion RBV -> Iso19139
    - Support du format RBV: 
      - Section Identification - complète sauf gestion des rôles multiples pour un contact
      - Section Métadonnées - complète


## Alimentation automatique du catalogue

### Objectif
L'objectif est de permettre à certains observatoires d'alimenter le portail de métadonnées automatiquement, c'est à dire sans passer par l'interface graphique. 
Ceci est adapté aux observatoires bénéficiant d'un SI comportant la totalité ou une partie des métadonnées mais n'étant pas moissonable. 
Ces observatoires peuvent générer leurs fiches de métadonnées au format RBV lorsque nécessaire et les intégrer automatiquement dans le portail. 
En cas de mise à jour la fiche de métadonnées, le système permet d'écraser la fiche existante par la nouvelle fiche. Ainsi une fiche peut être générée et mise dans le portail même si elle est dans un état incomplet et être mise à jour ultérieurement avec plus d'informations.  

### Notion d'identifiant unique
Les fiches de métadonnées sont identifiés par un identifiant unique dénommé uuid (cf. http://fr.wikipedia.org/wiki/Universal_Unique_Identifier). 
Pour un observatoire souhaitant alimenter le portail de manière automatique, il est obligatoire d'être capable de conserver dans son système d'informations l'uuid correspondant à une fiche de métadonnées.

Comme indiqué ci-dessous, il est possible de transmettre au portail une fiche sans uuid. Le portail en générera un au moment de l'integration et le transmettra dans la réponse afin qu'ils soit conservé par le système d'information de l'observatoire.

### Utilisateur spécifique
Le portail comporte un mécanisme permettant de gérer les droits sur les fiches afin de ne pas permettre l'exécution d'actions sur des fiches créées par d'autres personnes.
Ainsi, afin d'utiliser le service d'alimentation automatique il est nécessaire de demander la mise en place d'un compte (identifiant/mot de passe) dédié.  
Cette demande s'effectue auprès de **francois.andre(at)obs-mip.fr**. 


### Mise en oeuvre

Le service peut uniquement être utilisé de manière *programative*. Il s'agit d'un web service comportant quatre méthodes :

- Méthodes principales
 - addWithoutId : ajoute une fiche au format RBV ne possédant pas d'uuid.
 - addOrUpdateWithId : ajoute ou modifie une fiche au format RBV possédant un uuid.
 - getObservatoryUuid : retourne le uuid d'un observatoire d'après son nom.
 - getExpertimentalSiteUuid : retourne le uuid d'un site expérimental d'après son nom et celui de son observatoire.
 
- Méthodes destinées à la vérification
 - exist : vérifie la présence d'une fiche de métadonnées dans le catalogue
 - getById : retourne le contenu de la fiche de métadonnées au format ISO19139
 - getPdfById : retourne le contenu de la fiche de métadonnées au format PDF
  
 
Ces méthodes sont détaillées ci-dessous :


#### Méthode ````addWithoutId````

**URL :**  

 - http://portailrbvws.sedoo.fr/rest/integration/addWithoutId
 

**Paramètres :** (méthode de type **POST**)

- **src** : Le contenu du fichier RBV.
- **format** : L'unique valeur actuellement possible est *RBV*
- **login** : identifiant du compte effectuant l'alimentation
- **password** : mot de passe du compte effectuant l'alimentation

*Remarque* : le MIME Type à utiliser est *application/x-www-form-urlencoded* 

**Valeur de retour :**

En cas de succès:

- Code HTTP: 200
- Valeur de la chaine: l'uuid de la fiche

En cas d'échec:  

- Code HTTP 4xx/5xx
- Valeur de la chaine: message d'erreur 


#### Méthode ````addOrUpdateWithId````

**URL :**

 - http://portailrbvws.sedoo.fr/rest/integration/addOrUpdateWithId
 

**Paramètres :** (méthode de type **POST**)

- **src** : Le contenu du fichier RBV.
- **format** : L'unique valeur actuellement possible est *RBV*
- **login** : identifiant du compte effectuant l'alimentation
- **password** : mot de passe du compte effectuant l'alimentation


*Remarque* : le MIME Type à utiliser est *application/x-www-form-urlencoded* 

**Valeur de retour :**

En cas de succès: 
- Code HTTP: 200

En cas d'échec:
- Code HTTP 4xx/5xx
- Valeur de la chaine: message d'erreur


#### Méthode ````getObservatoryUuid````

**URL :**

 - http://portailrbvws.sedoo.fr/rest/integration/getObservatoryUuid

Exemple : 
 
 - http://portailrbvws.sedoo.fr/rest/integration/getObservatoryUuid?observatoryName=MEDYCYSS 
 

**Paramètres :** (méthode de type **GET**)

- **observatoryName** : Le nom de l'observatoire

**Valeur de retour :**

En cas de succès: 
- Valeur de la chaine: l'uuid de l'observatoire

En cas d'échec:
- Code HTTP 4xx/5xx
- Valeur de la chaine: message d'erreur

#### Méthode ````getExpertimentalSiteUuid````

**URL :**

 - http://portailrbvws.sedoo.fr/rest/integration/getExpertimentalSiteUuid
 
Exemple : 
 
 - http://portailrbvws.sedoo.fr/rest/integration/getExpertimentalSiteUuid?observatoryName=MEDYCYSS&experimentalSiteName=LEZ 
 

**Paramètres :** (méthode de type **GET**)

- **observatoryName** : Le nom de l'observatoire
- **experimentalSiteName** : Le nom du site expérimental

**Valeur de retour :**

En cas de succès: 
- Valeur de la chaine: l'uuid du site expérimental

En cas d'échec:
- Code HTTP 4xx/5xx
- Valeur de la chaine: message d'erreur


#### Méthode ````exist```` 

**URL :**

 - http://portailrbvws.sedoo.fr/rest/integration/exist/[uuid]

**Paramètres :** (méthode de type **GET**)

- **uuid** : L'uuid de la fiche dont on veut tester l'existence

**Valeur de retour :**

En cas de présence: 
- Valeur de la chaine: *true*

En cas d'échec ou d'absence:
- Valeur de la chaine: *false*


#### Méthode ````getById```` 

**URL :**

 - http://portailrbvws.sedoo.fr/rest/integration/getById/[uuid]

**Paramètres :** (méthode de type **GET**)

- **uuid** : L'uuid de la fiche dont on veut récupérer le contenu

**Valeur de retour :**

En cas de succès: 
- Code HTTP: 200
- Valeur de la chaine: le contenu de la fiche au format ISO19139. 

En cas d'échec:
- Code HTTP 4xx/5xx
- Valeur de la chaine: message d'erreur

#### Méthode ````getPdfById```` 

**URL :**

 - http://portailrbvws.sedoo.fr/rest/integration/getPdfById/[langue]/[uuid]

**Paramètres :** (méthode de type **GET**)

- **langue** : La langue à utiliser pour les libellés: Anglais: ````eng```` - Français: ````fre````
- **uuid** : L'uuid de la fiche dont on veut récupérer le contenu

**Valeur de retour :**

En cas de succès: 
- Code HTTP: 200
- Valeur de la chaine: le contenu de la fiche au format PDF. 

En cas d'échec:
- Code HTTP 4xx/5xx
- Valeur de la chaine: message d'erreur

Remarque: cette fonction permet de contituer une URL pérenne vers une fiche d'identifiant connu (ex: http://portailrbvws.sedoo.fr/rest/Integration/getPdfById/eng/1a498b36-d255-464a-aa5a-f65e0e885a7e)


Voici un exemple d'appel du service en java (utilisation la la librairie *Jersey*)
````
private static URI getBaseURI() {
		return UriBuilder.fromUri("http://sedoo.sedoo.fr/metadataws").build();
	}
	
	/**
	 * Test effectuant un certain nombre d'actions élémentaires d'alimentation du portail:
	 *  - Création d'une fiche au format RBV
	 *  - Intégration dans le portail
	 *  - Récupération de l'uuid
	 *  - Vérification de la présence et du contenu
	 *  - Modification  de la fiche
	 *  - Mise à jour dans le portail
	 *  - Vérification du contenu modifié
	 *  
	 * @throws Exception
	 */
	@Test
	public void testBasicLifeCycle() throws Exception
	{
		RBVMetadata metadata = MetadataSampleProvider.getTestMetadata();
		String content = RBVTools.toRBV(metadata);
		MultivaluedMap formData = new MultivaluedMapImpl();
		formData.add("src", content);
		formData.add("format", IntegrationService.RBV_FORMAT_NAME);
		formData.add("login", "test");
		formData.add("password", "sedootest");
		ClientConfig config = new DefaultClientConfig();
		Client client = Client.create(config);
		WebResource service = client.resource(getBaseURI());
		ClientResponse response = service.path("rest").path("Integration").path("addWithoutId").type(MediaType.APPLICATION_FORM_URLENCODED_TYPE).post(ClientResponse.class, formData);
		int status = response.getStatus();
		Assert.assertEquals("L'opération doit s'être bien déroulée", HttpStatus.SC_OK, status);
		String uuid = response.getEntity(String.class);
		Assert.assertNotNull("L'identifiant doit être présent", uuid);
		String result = service.path("rest").path("Integration").path("exist").path(uuid).accept(MediaType.TEXT_HTML).get(String.class).toString();
		Assert.assertEquals("La fiche doit exister", "true", result);
		String rawXML = service.path("rest").path("Integration").path("getById").path(uuid).accept(MediaType.TEXT_HTML).get(String.class).toString();
		MetadataSummary summary = MetadataSummary.fromXML(rawXML);
		Assert.assertEquals("Les données doivent correspondre", metadata.getResourceTitle(), summary.getResourceTitle());
		metadata.setResourceTitle("Updated "+metadata.getResourceTitle());
		metadata.setUuid(uuid);
		content = RBVTools.toRBV(metadata);
		formData = new MultivaluedMapImpl();
		formData.add("src", content);
		formData.add("format", IntegrationService.RBV_FORMAT_NAME);
		formData.add("login", "test");
		formData.add("password", "sedootest");
		response = service.path("rest").path("Integration").path("addOrUpdateWithId").type(MediaType.APPLICATION_FORM_URLENCODED_TYPE).post(ClientResponse.class, formData);
		status = response.getStatus();
		Assert.assertEquals("L'opération doit s'être bien déroulée", HttpStatus.SC_OK, status);
		rawXML = service.path("rest").path("Integration").path("getById").path(uuid).accept(MediaType.TEXT_HTML).get(String.class).toString();
		summary = MetadataSummary.fromXML(rawXML);
		Assert.assertEquals("Les données doivent correspondre", metadata.getResourceTitle(), summary.getResourceTitle());
	}
````

## Conversion RBV-> ISO19139

### Objectif
L'objectif est de permettre à certains observatoires de transformer des fiches du format RBV au format ISO19139.

### Mise en oeuvre 

Le service de conversion RBV vers ISO19139 peut être testé en ligne [ici](../wstest.html). Sur cette page, on peut soit consulter le résultat de la conversion directement sur la page ou via un fichier XML téléchargé.

Le service peut également être utilisé de manière *programative*. Il s'agit d'un web service de type **POST** disponible aux adresses suivantes :

 - http://sedoo.sedoo.fr/metadataws/rest/RBVTo19139/post/text: récupère le format ISO19139 sous forme de chaine de caractères.
 - http://sedoo.sedoo.fr/metadataws/rest/RBVTo19139/post/download: récupère le format ISO19139 sous forme de chaine de fichier.
 
**Paramètres :**

- **src**  : Le contenu du fichier RBV.    

Voici un exemple d'appel du service en java (utilisation la la librairie *Jersey*)

````
	private static URI getBaseURI() {
		return UriBuilder.fromUri("http://sedoo.sedoo.fr/metadataws").build();
	}

	@Test
	public void testPostDownload() throws Exception
	{
		System.out.println("-- test Postdownload --");
		MultivaluedMap formData = new MultivaluedMapImpl();
		Metadata metadata = TestMetadataDAO.getTestMetadata();
		String str1 = RBVTools.toRBV(metadata);
		formData.add("src", str1);
		ClientConfig config = new DefaultClientConfig();
		Client client = Client.create(config);
		WebResource service = client.resource(getBaseURI());
		ClientResponse response = service.path("rest").path("RBVTo19139").path("post").path("download").type(MediaType.APPLICATION_FORM_URLENCODED_TYPE).post(ClientResponse.class, formData);
		File s= response.getEntity(File.class);
		String readFileToString = FileUtils.readFileToString(s);
		System.out.println(readFileToString);
	}
	
	@Test
	public void testPostText() throws Exception
	{
		System.out.println("-- test PostText --");
		MultivaluedMap formData = new MultivaluedMapImpl();
		Metadata metadata = TestMetadataDAO.getTestMetadata();
		String str1 = RBVTools.toRBV(metadata);
		formData.add("src", str1);
		ClientConfig config = new DefaultClientConfig();
		Client client = Client.create(config);
		WebResource service = client.resource(getBaseURI());
		ClientResponse response = service.path("rest").path("RBVTo19139").path("post").path("text").type(MediaType.APPLICATION_FORM_URLENCODED_TYPE).post(ClientResponse.class, formData);
		String isoString = response.getEntity(String.class);
		
		System.out.println(isoString);
	}
```` 



  

